package com.ingress.springsecurity.controller;

import com.ingress.springsecurity.dto.RegisterRequestDto;
import com.ingress.springsecurity.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
public class UserController {
    private final UserService userService;

    @GetMapping("/register")
    public ResponseEntity<String> register() {
        return ResponseEntity.ok("Successfully registered");
    }
}
