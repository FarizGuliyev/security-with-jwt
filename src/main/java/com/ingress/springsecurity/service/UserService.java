package com.ingress.springsecurity.service;

import com.ingress.springsecurity.dto.RegisterRequestDto;
import com.ingress.springsecurity.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;


    public void register(RegisterRequestDto dto) {

    }
}
